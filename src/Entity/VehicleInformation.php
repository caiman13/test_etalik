<?php

namespace App\Entity;

use App\Repository\VehicleInformationRepository;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: VehicleInformationRepository::class)]
class VehicleInformation
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\Column(length: 255, nullable: true)]
    private ?string $businessAcount = null;

    #[ORM\Column(length: 255, nullable: true)]
    private ?string $eventAcount = null;

    #[ORM\Column(length: 255, nullable: true)]
    private ?string $lastEventAcount = null;

    #[ORM\Column(length: 255, nullable: true)]
    private ?string $sheetNumber = null;

    #[ORM\Column(length: 255, nullable: true)]
    private ?string $civility = null;

    #[ORM\Column(length: 255, nullable: true)]
    private ?string $actualOwner = null;

    #[ORM\Column(length: 255, nullable: true)]
    private ?string $lastName = null;

    #[ORM\Column(length: 255, nullable: true)]
    private ?string $firstName = null;

    #[ORM\Column(length: 255, nullable: true)]
    private ?string $streetNumber = null;

    #[ORM\Column(length: 255, nullable: true)]
    private ?string $additionalAddress = null;

    #[ORM\Column(length: 255, nullable: true)]
    private ?string $postalCode = null;

    #[ORM\Column(length: 255, nullable: true)]
    private ?string $city = null;

    #[ORM\Column(length: 255, nullable: true)]
    private ?string $homePhone = null;

    #[ORM\Column(length: 255, nullable: true)]
    private ?string $portablePhone = null;

    #[ORM\Column(length: 255, nullable: true)]
    private ?string $jobPhone = null;

    #[ORM\Column(length: 255, nullable: true)]
    private ?string $email = null;

    #[ORM\Column(type: Types::DATE_MUTABLE, nullable: true)]
    private ?\DateTimeInterface $circulationDate = null;

    #[ORM\Column(type: Types::DATE_MUTABLE, nullable: true)]
    private ?\DateTimeInterface $purchaseDate = null;

    #[ORM\Column(type: Types::DATE_MUTABLE, nullable: true)]
    private ?\DateTimeInterface $firstEventDate = null;

    #[ORM\Column(length: 255, nullable: true)]
    private ?string $brand = null;

    #[ORM\Column(length: 255, nullable: true)]
    private ?string $model = null;

    #[ORM\Column(length: 255, nullable: true)]
    private ?string $version = null;

    #[ORM\Column(length: 255, nullable: true)]
    private ?string $vin = null;

    #[ORM\Column(length: 255, nullable: true)]
    private ?string $resgistrationNumber = null;

    #[ORM\Column(length: 255, nullable: true)]
    private ?string $prospectType = null;

    #[ORM\Column(length: 255, nullable: true)]
    private ?string $mileage = null;

    #[ORM\Column(length: 255, nullable: true)]
    private ?string $energie = null;

    #[ORM\Column(length: 255, nullable: true)]
    private ?string $sellerVn = null;

    #[ORM\Column(length: 255, nullable: true)]
    private ?string $sellerVo = null;

    #[ORM\Column(length: 255, nullable: true)]
    private ?string $factureComment = null;

    #[ORM\Column(length: 255, nullable: true)]
    private ?string $typeVnVo = null;

    #[ORM\Column(length: 255, nullable: true)]
    private ?string $fileVnVoNumber = null;

    #[ORM\Column(length: 255, nullable: true)]
    private ?string $salesIntermediary = null;

    #[ORM\Column(type: Types::DATE_MUTABLE, nullable: true)]
    private ?\DateTimeInterface $eventDate = null;

    #[ORM\Column(length: 255, nullable: true)]
    private ?string $origineEvent = null;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getBusinessAcount(): ?string
    {
        return $this->businessAcount;
    }

    public function setBusinessAcount(?string $businessAcount): static
    {
        $this->businessAcount = $businessAcount;

        return $this;
    }

    public function getEventAcount(): ?string
    {
        return $this->eventAcount;
    }

    public function setEventAcount(?string $eventAcount): static
    {
        $this->eventAcount = $eventAcount;

        return $this;
    }

    public function getLastEventAcount(): ?string
    {
        return $this->lastEventAcount;
    }

    public function setLastEventAcount(?string $lastEventAcount): static
    {
        $this->lastEventAcount = $lastEventAcount;

        return $this;
    }

    public function getSheetNumber(): ?string
    {
        return $this->sheetNumber;
    }

    public function setSheetNumber(?string $sheetNumber): static
    {
        $this->sheetNumber = $sheetNumber;

        return $this;
    }

    public function getCivility(): ?string
    {
        return $this->civility;
    }

    public function setCivility(?string $civility): static
    {
        $this->civility = $civility;

        return $this;
    }

    public function getActualOwner(): ?string
    {
        return $this->actualOwner;
    }

    public function setActualOwner(?string $actualOwner): static
    {
        $this->actualOwner = $actualOwner;

        return $this;
    }

    public function getLastName(): ?string
    {
        return $this->lastName;
    }

    public function setLastName(?string $lastName): static
    {
        $this->lastName = $lastName;

        return $this;
    }

    public function getFirstName(): ?string
    {
        return $this->firstName;
    }

    public function setFirstName(?string $firstName): static
    {
        $this->firstName = $firstName;

        return $this;
    }

    public function getStreetNumber(): ?string
    {
        return $this->streetNumber;
    }

    public function setStreetNumber(?string $streetNumber): static
    {
        $this->streetNumber = $streetNumber;

        return $this;
    }

    public function getAdditionalAddress(): ?string
    {
        return $this->additionalAddress;
    }

    public function setAdditionalAddress(?string $additionalAddress): static
    {
        $this->additionalAddress = $additionalAddress;

        return $this;
    }

    public function getPostalCode(): ?string
    {
        return $this->postalCode;
    }

    public function setPostalCode(?string $postalCode): static
    {
        $this->postalCode = $postalCode;

        return $this;
    }

    public function getCity(): ?string
    {
        return $this->city;
    }

    public function setCity(?string $city): static
    {
        $this->city = $city;

        return $this;
    }

    public function getHomePhone(): ?string
    {
        return $this->homePhone;
    }

    public function setHomePhone(?string $homePhone): static
    {
        $this->homePhone = $homePhone;

        return $this;
    }

    public function getPortablePhone(): ?string
    {
        return $this->portablePhone;
    }

    public function setPortablePhone(?string $portablePhone): static
    {
        $this->portablePhone = $portablePhone;

        return $this;
    }

    public function getJobPhone(): ?string
    {
        return $this->jobPhone;
    }

    public function setJobPhone(?string $jobPhone): static
    {
        $this->jobPhone = $jobPhone;

        return $this;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(?string $email): static
    {
        $this->email = $email;

        return $this;
    }

    public function getCirculationDate(): ?\DateTimeInterface
    {
        return $this->circulationDate;
    }

    public function setCirculationDate(?\DateTimeInterface $circulationDate): static
    {
        $this->circulationDate = $circulationDate;

        return $this;
    }

    public function getPurchaseDate(): ?\DateTimeInterface
    {
        return $this->purchaseDate;
    }

    public function setPurchaseDate(?\DateTimeInterface $purchaseDate): static
    {
        $this->purchaseDate = $purchaseDate;

        return $this;
    }

    public function getFirstEventDate(): ?\DateTimeInterface
    {
        return $this->firstEventDate;
    }

    public function setFirstEventDate(?\DateTimeInterface $firstEventDate): static
    {
        $this->firstEventDate = $firstEventDate;

        return $this;
    }

    public function getBrand(): ?string
    {
        return $this->brand;
    }

    public function setBrand(?string $brand): static
    {
        $this->brand = $brand;

        return $this;
    }

    public function getModel(): ?string
    {
        return $this->model;
    }

    public function setModel(?string $model): static
    {
        $this->model = $model;

        return $this;
    }

    public function getVersion(): ?string
    {
        return $this->version;
    }

    public function setVersion(?string $version): static
    {
        $this->version = $version;

        return $this;
    }

    public function getVin(): ?string
    {
        return $this->vin;
    }

    public function setVin(?string $vin): static
    {
        $this->vin = $vin;

        return $this;
    }

    public function getResgistrationNumber(): ?string
    {
        return $this->resgistrationNumber;
    }

    public function setResgistrationNumber(?string $resgistrationNumber): static
    {
        $this->resgistrationNumber = $resgistrationNumber;

        return $this;
    }

    public function getProspectType(): ?string
    {
        return $this->prospectType;
    }

    public function setProspectType(?string $prospectType): static
    {
        $this->prospectType = $prospectType;

        return $this;
    }

    public function getMileage(): ?string
    {
        return $this->mileage;
    }

    public function setMileage(?string $mileage): static
    {
        $this->mileage = $mileage;

        return $this;
    }

    public function getEnergie(): ?string
    {
        return $this->energie;
    }

    public function setEnergie(?string $energie): static
    {
        $this->energie = $energie;

        return $this;
    }

    public function getSellerVn(): ?string
    {
        return $this->sellerVn;
    }

    public function setSellerVn(?string $sellerVn): static
    {
        $this->sellerVn = $sellerVn;

        return $this;
    }

    public function getSellerVo(): ?string
    {
        return $this->sellerVo;
    }

    public function setSellerVo(?string $sellerVo): static
    {
        $this->sellerVo = $sellerVo;

        return $this;
    }

    public function getFactureComment(): ?string
    {
        return $this->factureComment;
    }

    public function setFactureComment(?string $factureComment): static
    {
        $this->factureComment = $factureComment;

        return $this;
    }

    public function getTypeVnVo(): ?string
    {
        return $this->typeVnVo;
    }

    public function setTypeVnVo(?string $typeVnVo): static
    {
        $this->typeVnVo = $typeVnVo;

        return $this;
    }

    public function getFileVnVoNumber(): ?string
    {
        return $this->fileVnVoNumber;
    }

    public function setFileVnVoNumber(?string $fileVnVoNumber): static
    {
        $this->fileVnVoNumber = $fileVnVoNumber;

        return $this;
    }

    public function getSalesIntermediary(): ?string
    {
        return $this->salesIntermediary;
    }

    public function setSalesIntermediary(?string $salesIntermediary): static
    {
        $this->salesIntermediary = $salesIntermediary;

        return $this;
    }

    public function getEventDate(): ?\DateTimeInterface
    {
        return $this->eventDate;
    }

    public function setEventDate(?\DateTimeInterface $eventDate): static
    {
        $this->eventDate = $eventDate;

        return $this;
    }

    public function getOrigineEvent(): ?string
    {
        return $this->origineEvent;
    }

    public function setOrigineEvent(?string $origineEvent): static
    {
        $this->origineEvent = $origineEvent;

        return $this;
    }
}
