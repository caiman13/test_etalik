<?php

namespace App\Repository;

use App\Entity\VehicleInformation;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<VehicleInformation>
 *
 * @method VehicleInformation|null find($id, $lockMode = null, $lockVersion = null)
 * @method VehicleInformation|null findOneBy(array $criteria, array $orderBy = null)
 * @method VehicleInformation[]    findAll()
 * @method VehicleInformation[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class VehicleInformationRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, VehicleInformation::class);
    }

//    /**
//     * @return VehicleInformation[] Returns an array of VehicleInformation objects
//     */
//    public function findByExampleField($value): array
//    {
//        return $this->createQueryBuilder('v')
//            ->andWhere('v.exampleField = :val')
//            ->setParameter('val', $value)
//            ->orderBy('v.id', 'ASC')
//            ->setMaxResults(10)
//            ->getQuery()
//            ->getResult()
//        ;
//    }

//    public function findOneBySomeField($value): ?VehicleInformation
//    {
//        return $this->createQueryBuilder('v')
//            ->andWhere('v.exampleField = :val')
//            ->setParameter('val', $value)
//            ->getQuery()
//            ->getOneOrNullResult()
//        ;
//    }
}
